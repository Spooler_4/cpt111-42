﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ScoreManager : MonoBehaviour
{

    public int lives = 20;
    public int money = 100;
    public int mana = 100;

    public Text moneyText;
    public Text livesText;
    public Text manaText;
    public int counter;

    GameObject[] defeatObjects;
    GameObject[] victoryObjects;


    void Start()
    {

        defeatObjects = GameObject.FindGameObjectsWithTag("Defeat");
        victoryObjects = GameObject.FindGameObjectsWithTag("Victory");
        hideMenu();
    }


    public void hideMenu()
    {
        foreach (GameObject g in defeatObjects)
        {
            g.SetActive(false);
        }
        foreach (GameObject g in victoryObjects)
        {
            g.SetActive(false);
        }
    }

    public void LoseLife(int l = 1)
    {
        lives -= l;
        if (lives <= 0)
        {
            GameOver();
        }
    }

    public void IncreaseLife()
    {

        lives++;
        if (lives >= 100)
        { Victory(); }

    }
    public void GameOver()
    {
        Debug.Log("Game Over");
        // TODO: Send the player to a game-over screen instead!
        foreach (GameObject g in defeatObjects)
        {
            g.SetActive(true);
            Time.timeScale = 0;
        }
    }


    public void Victory()
    {
        Debug.Log("Player is victorious");
        // TODO: Send the player to a game-over screen instead!
        foreach (GameObject g in victoryObjects)
        {
            g.SetActive(true);
            Time.timeScale = 0;
        }
    }

    void Update()
    {
        // FIXME: This doesn't actually need to update the text every frame.
        moneyText.text = "Gold: " + money.ToString();
        livesText.text = "Completion: " + lives.ToString() + "%";
        manaText.text = "Mana: " + mana.ToString();
        counter++;
        if (counter ==1000)
        {
            counter = 0;
            mana += 50;
            IncreaseLife();
        }

    }

}
