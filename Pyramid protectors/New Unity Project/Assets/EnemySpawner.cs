﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{

    public double initialDelay;     //Time before enemies start to spawn
    public double initialTimer;     //Initial time between spawns
    public int initialAmount;       //Initial amount of enemies spawned
    public double timerGainTime;    //Time after which the time between spawns is decreased
    public double timerGainPerc;    //Amount the time between spawns decreases
    public double amountGainTime;   //Time after which the amount of enemies spawned increases
    public int amountGain;          //Amount of enemies increased per spawn
    public Object enemy;            //Enemy prefab
    public Vector3 spawnPos;        //Spawn position
    double lastSpawn;               //Time when an enemy was last spawned
    double timer;                   //Time until the next spawn
    double timerGainTimer;          //Time since last time decrease
    double amountGainTimer;         //Time since last amount increase

    //Debug
    public double currentTime;
    public double nextSpawnTime;
    public double lastSpawnTime;

    // Use this for initialization
    void Start()
    {
        lastSpawn = 0;
        timer = initialDelay;
        timerGainTimer = initialDelay;
        amountGainTimer = initialDelay;
    }

    // Update is called once per frame
    void Update()
    {

        if (Time.time - lastSpawn >= timer)
        {
            for (int i = 0; i < initialAmount; i++)
            {
                Object.Instantiate(enemy, spawnPos + new Vector3(0, 0, 5 * Random.value), Quaternion.identity);
            }
            lastSpawn = Time.time;
            timer = initialTimer;
            nextSpawnTime = Time.time + timer;  //Debug
        }

        if (Time.time - timerGainTimer >= timerGainTime)
        {
            initialTimer *= timerGainPerc;
            timerGainTimer = Time.time;
        }

        if (Time.time - amountGainTimer >= amountGainTime)
        {
            initialAmount += amountGain;
            amountGainTimer = Time.time;
        }


        //Debug
        currentTime = Time.time;
        lastSpawnTime = lastSpawn;

    }
}
