﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TowerUI : MonoBehaviour
{

    public GameObject tower;
    public GameObject towerSpot;
    public int costMultiplier;
    public float damageMultiplier;

    public int cost;
    public float damage;
    public int spent;

    public Text sellText;
    public Text upgradeText;

    // Use this for initialization
    void Start()
    {

        cost = tower.GetComponent<Tower>().cost;
        damage = tower.GetComponent<Tower>().damage;
        spent = tower.GetComponent<Tower>().spent;
        

    }

    // Update is called once per frame
    void Update()
    {
        sellText.text = "Sell for \n"+ (3 * (spent / 4)) + " gold";
        upgradeText.text = "Upgrade for "+(cost*costMultiplier)+" gold\n+"+((damage*damageMultiplier)-damage)+" damage";
    }

    public void upgrade()
    {
        cost = tower.GetComponent<Tower>().cost;
        damage = tower.GetComponent<Tower>().damage;
        spent = tower.GetComponent<Tower>().spent;

        ScoreManager sm = GameObject.FindObjectOfType<ScoreManager>();
        if (sm.money >= (cost * costMultiplier))
        {

            tower.GetComponent<Tower>().cost = cost * costMultiplier;
            cost = tower.GetComponent<Tower>().cost;
            sm.money -= cost;

            tower.GetComponent<Tower>().spent = spent + cost;
            spent = tower.GetComponent<Tower>().spent;

            tower.GetComponent<Tower>().damage = damage * damageMultiplier;
            damage = tower.GetComponent<Tower>().damage;

            GameObject.Destroy(gameObject);


        }
    }

    public void sell()
    {

        cost = tower.GetComponent<Tower>().spent;
        ScoreManager sm = GameObject.FindObjectOfType<ScoreManager>();
        sm.money += (3*(spent/4));
        GameObject.Instantiate(towerSpot, tower.transform.position, Quaternion.identity);
        GameObject.Destroy(tower);
        GameObject.Destroy(gameObject);

    }
}
