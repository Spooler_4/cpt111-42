﻿using UnityEngine;
using System.Collections;

public class SpellManager : MonoBehaviour {

    public int mana;
    public bool starfall = false;
    public Ray ray;
    public RaycastHit hit;

	// Use this for initialization
	void Start () {
        

    }
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetMouseButtonDown(0)) {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out hit))
            {
                if (starfall == true)
                {
                    Collider[] cols = Physics.OverlapSphere(hit.point, 10);

                    foreach (Collider c in cols)
                    {
                        Enemy e = c.GetComponent<Enemy>();
                        if (e != null)
                        {
                            e.GetComponent<Enemy>().TakeDamage(125 * ((10 - Vector3.Distance(hit.point, e.transform.position)) / 10));
                        }
                    }
                    starfall = false;
                    gameObject.GetComponent<ScoreManager>().mana -= 150;
                }
            }
            
        }
	}

    public void starfallSelection(bool var) {
        mana = gameObject.GetComponent<ScoreManager>().mana;
        if(mana > 150) {
            starfall = var;
        }
        
    }

    public void executionSelection() {
        mana = gameObject.GetComponent<ScoreManager>().mana;
        if(mana > 500) {
            Collider[] cols = Physics.OverlapSphere(new Vector3(0,0,0), 100);

            foreach (Collider c in cols)
            {
                Enemy e = c.GetComponent<Enemy>();
                if (e != null)
                {
                    if(e.GetComponent<Enemy>().health <= 15) {
                        e.GetComponent<Enemy>().TakeDamage(500);
                    }
                }
            }
            gameObject.GetComponent<ScoreManager>().mana -= 500;
        }
    }

    public void scientificAdvancementSelection() {
        mana = gameObject.GetComponent<ScoreManager>().mana;
        if(mana > 750) {
            gameObject.GetComponent<ScoreManager>().lives += 19;
            gameObject.GetComponent<ScoreManager>().IncreaseLife();
            gameObject.GetComponent<ScoreManager>().mana -= 750;
        }        
    }
}
