﻿using UnityEngine;
using System.Collections;

public class TowerButtons : MonoBehaviour {

    public GameObject towerButtons;
    GameObject buttons;
    bool open = false;
    bool time = false;
    

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if(time == true && Input.GetMouseButtonUp(0)) {

            open = false;
            time = false;
            GameObject.Destroy(buttons);

        }


        if (open == true && Input.GetMouseButtonDown(0)) {

            time = true;

        }


	}

    void OnMouseUp() {

        open = true;
        buttons = GameObject.Instantiate(towerButtons,gameObject.transform.position,Quaternion.identity) as GameObject;
        buttons.transform.Rotate(70,270,0);
        buttons.GetComponent<TowerUI>().tower = gameObject;

    }
}
