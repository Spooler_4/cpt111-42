﻿using UnityEngine;
using System.Collections;

public class UIManScript : MonoBehaviour {

    public void StartGame()
    {
        Application.LoadLevel("MiniGame");
    }

    public void StartLevel2()
    {
        Application.LoadLevel("Level 2");
    }

    public void SelectLvl()
    {
        Application.LoadLevel("MenuSelectLvl");
    }

    public void Settings()
    {
        Application.LoadLevel("MenuSettings");
    }

    //TODO: get this to actually work. Current code
    // does nothing.
    public void ExitGame()
    {
        Application.LoadLevel("MenuConfirmation");
    }

    public void ExitGameYes()
    {
        Application.Quit();
    }

    public void Back()
    {
        Application.LoadLevel("MenuMain");
    }
}
